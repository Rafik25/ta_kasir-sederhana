import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Balok {
    private JButton button1;
    private JButton tfKeluar;
    private JTextField tfPanjang;
    private JTextField tfLebar;
    private JTextField tfTinggi;
    private JTextField tfVolume;
    private JTextField tfLuasPermukaan;
    private JPanel a;

    public Balok() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double panjang, lebar, tinggi, volume, luaspermukaan;

                panjang = Double.parseDouble(tfPanjang.getText());
                lebar = Double.parseDouble(tfLebar.getText());
                tinggi = Double.parseDouble(tfTinggi.getText());

                volume = panjang*lebar*tinggi;
                luaspermukaan = 2*(panjang*lebar + panjang*tinggi + lebar*tinggi);

                tfVolume.setText(String.valueOf(volume));
                tfLuasPermukaan.setText(String.valueOf(luaspermukaan));
            }
        });
        tfKeluar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Balok");
        frame.setContentPane(new Balok().a);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
